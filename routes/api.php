<?php

\Illuminate\Support\Facades\Route::post('api/exception-handler', function (\Illuminate\Http\Request $request) {
    (new \BeInteractive\BeManager\Library\Client())->sendException('js', $request->input('message'), $request->input('stack'));
});