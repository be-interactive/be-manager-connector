## BE-Manager Connector
Everything to connect to the BE-Manager API

###Setup
Run this command:
```shell
php artisan vendor:publish --provider="BeInteractive\BeManager\BeManagerServiceProvider"
```

Add these to your .env
```dotenv
BE_MANAGER_ENDPOINT=https://manager.beinter.nl // only required if this is different then normally
BE_MANAGER_TOKEN="token" // API token for the current site
```

Add to your Console Kernel
```php
$schedule->command('be-manager:heartbeat')->everyFiveMinutes();
$schedule->command('be-manager:disk-usage')->daily();
```

To handle exceptions in php add the following to your global error handler:
```php
// $e should be an implementation of Throwable
\BeInteractive\BeManager\Library\ExceptionHandler::handle($e);
```

To handle exceptions in javascript add the following at the start of your javascript entrypoint
```js
import 'exception-handler'; // Or the correct path the the published file
```
