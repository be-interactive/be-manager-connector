<?php

namespace BeInteractive\BeManager\Library;

use Illuminate\Support\Facades\Log;
use Throwable;

class ExceptionHandler
{

    public static function handle(Throwable $throwable) {
        try {
            if (!empty(config('be_manager.token'))) {
                (new Client())->sendException('php', $throwable->getMessage(), $throwable->getTraceAsString());
            }
        } catch (Throwable $e) {
            Log::error('Sending exception to BE-Manager failed: ' . $e->getMessage() . "\n" . $throwable->getTraceAsString());
        }
    }
}