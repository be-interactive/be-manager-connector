<?php

namespace BeInteractive\BeManager\Library;

use Exception;
use Illuminate\Support\Facades\Http;

class Client
{

    /**
     * @throws Exception
     */
    public function __construct()
    {
        if (is_null(config('be_manager.token'))) {
            throw new Exception('BE-Manager token not found');
        }
    }

    public function sendHeartbeat($amount = 1): Client
    {
        Http::withToken(config('be_manager.token'))
            ->acceptJson()
            ->post(config('be_manager.endpoint') . '/api/register-log', [
                'type' => 'heartbeat',
                'amount' => $amount,
            ]);

        return $this;
    }

    public function sendDiskUsage(float | int $usage): Client
    {
        Http::withToken(config('be_manager.token'))
            ->acceptJson()
            ->post(config('be_manager.endpoint') . '/api/register-log', [
                'type' => 'disk_usage',
                'amount' => $usage,
            ]);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function sendException(string $type, string $message, ?string $trace = null): Client
    {
        if ($type !== 'php' && $type !== 'js') {
            throw new Exception('Allowed exception types are php and js');
        }

        Http::withToken(config('be_manager.token'))
            ->acceptJson()
            ->post(config('be_manager.endpoint') . '/api/register-log', [
                'type' => $type . '_error',
                'description' => $message,
                'additional' => [
                    'stack' => $trace,
                ]
            ]);

        return $this;
    }
}
