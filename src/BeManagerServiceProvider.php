<?php

namespace BeInteractive\BeManager;

use BeInteractive\BeManager\Console\Commands\RegisterDiskUsageCommand;
use BeInteractive\BeManager\Console\Commands\RegisterHeartbeatCommand;
use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;

class BeManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     * @throws \Exception
     */
    public function boot()
    {
        if (File::exists(base_path('.env')) && empty(config('be_manager.token')) && config('app.env') === 'production') {
            throw new \Exception('BE-Manager Token is required.');
        }

        $this->publishes([
            __DIR__ . '/../config/be_manager.php' => config_path('be_manager.php'),
        ]);

        $this->publishes([
            __DIR__.'/../resources/js/' => resource_path('js')
        ]);

        if ($this->app->runningInConsole()) {
            $this->commands([
                RegisterHeartbeatCommand::class,
                RegisterDiskUsageCommand::class,
            ]);
        }
    }
}
