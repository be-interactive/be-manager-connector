<?php

namespace BeInteractive\BeManager\Console\Commands;

use BeInteractive\BeManager\Library\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class RegisterDiskUsageCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'be-manager:disk-usage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Registers the current disk usage to the BE-Manager app.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $file_size = 0;

        foreach( File::allFiles(storage_path('app')) as $file)
        {
            $file_size += $file->getSize();
        }

        $file_size = number_format($file_size / 1048576,2);

        (new Client())->sendDiskUsage($file_size);

        return 0;
    }
}
