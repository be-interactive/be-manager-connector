<?php

namespace BeInteractive\BeManager\Console\Commands;

use BeInteractive\BeManager\Library\Client;
use Illuminate\Console\Command;

class RegisterHeartbeatCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'be-manager:heartbeat {amount}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Registers a heartbeat to the BE-Manager app.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        (new Client())->sendHeartbeat($this->argument('amount'));

        return 0;
    }
}
