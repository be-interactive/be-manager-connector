import axios from 'axios';

window.onerror = function (message, file, line, col, error) {
    try {
        axios.post('/api/exception-handler', {
            message: message,
            stack: error
        }).catch(e => {
            console.error(e);
        });
    } catch (e) {
        console.error(e);
    }
};

window.addEventListener("error", function (e) {
    try {
        axios.post('/api/exception-handler', {
            message: e.message,
            stack: e.error
        }).catch(e => {
            console.error(e);
        });
    } catch (e) {
        console.error(e);
    }
});

window.addEventListener('unhandledrejection', function (e) {
    try {
        axios.post('/api/exception-handler', {
            message: e.reason.message,
            stack: e.reason.stack
        }).catch(e => {
            console.error(e);
        });
    } catch (e) {
        console.error(e);
    }
});
