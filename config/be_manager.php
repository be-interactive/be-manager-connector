<?php

return [
    'endpoint' => env('BE_MANAGER_ENDPOINT', 'https://manager.beinter.nl/'),
    'token' => env('BE_MANAGER_TOKEN', null),
];
